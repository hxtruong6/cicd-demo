docker run --rm -it -v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner register
# 1. Enter the GitLab instance URL (for example, https://gitlab.com/):
# https://gitlab.com/
# 2. Enter the registration token:
# oVsjhgbsypUSterzxUXJ                                  --> get from repository: setting->cicd->runners (in Specific runners)
# 3. Enter a description for the runner:
# [f35df9c25c98]: cmc runner
# 4. Enter tags for the runner (comma-separated):
# demo1, demo2, demo3
# 5. Registering runner... succeeded                     runner=oVsjhgbs
# 6. Enter an executor: docker, docker-ssh, parallels, virtualbox, docker+machine, custom, ssh, docker-ssh+machine, kubernetes, shell:
# docker
# 7. Enter the default Docker image (for example, ruby:2.6):
# ubuntu:latest
# Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!